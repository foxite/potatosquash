﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

namespace PotatoSquash
{
	public class PlayerMovement : Singleton<PlayerMovement>
	{
		[SerializeField] private AudioClip[] m_WalkClips;

		private AudioSource m_AudioSource;
		private TileCountData m_TCD;
		private Tilemap m_Tilemap;
		private Animator m_Animator;

		private bool m_UseWASD;
		private bool m_startgame;

		public bool m_EnableAudio;

		private Vector3 m_InitialPosition;

		private void Start()
		{
			UpdateSettings();
			m_AudioSource = GetComponent<AudioSource>();
			m_Animator = GetComponent<Animator>();

			SceneManager.sceneLoaded += (scene, mode) =>
			{
				m_TCD = FindObjectOfType<TileCountData>();
				m_Tilemap = m_TCD.GetComponentInChildren<Tilemap>();
				transform.position = GameObject.FindGameObjectWithTag("Respawn").transform.position;
				m_InitialPosition = transform.position;
			};

			m_InitialPosition = transform.position;
		}

		public void ResetPlayer()
		{
			transform.position = m_InitialPosition;
		}

		private void Update()
		{
			if (this.m_startgame)
			{
				if (m_UseWASD)
				{
					KeyBoardPlayerMovement();
				}
			}
		}

		private void KeyBoardPlayerMovement()
		{
			if (Input.GetKeyDown(KeyCode.W))
			{
				MovePlayerUp();
			}
			else if (Input.GetKeyDown(KeyCode.S))
			{
				MovePlayerDown();
			}
			else if (Input.GetKeyDown(KeyCode.A))
			{
				MovePlayerLeft();
			}
			else if (Input.GetKeyDown(KeyCode.D))
			{
				MovePlayerRight();
			}
		}

		public void MovePlayerUp() => MovePlayer(Vector2Int.up);
		public void MovePlayerLeft() => MovePlayer(Vector2Int.left);
		public void MovePlayerRight() => MovePlayer(Vector2Int.right);
		public void MovePlayerDown() => MovePlayer(Vector2Int.down);

		private void MovePlayer(Vector2Int delta)
		{
			if (this.m_startgame)
			{
				float direction = Mathf.Atan2(delta.y, delta.x) / (2 * Mathf.PI) + 0.5f;
				m_Animator.SetFloat("Direction", direction);

				if (m_Tilemap == null)
				{
					transform.Translate(delta.x, delta.y, 0);
				}
				else
				{
					Vector3Int cellPosition = m_Tilemap.WorldToCell(transform.position);

					if (m_TCD.CanStepOnTile(new Vector2Int(cellPosition.x, cellPosition.y) + delta))
					{
						RandomAudioClip();
						transform.Translate(delta.x, delta.y, 0);
						m_TCD.IncrementCount(new Vector2Int(cellPosition.x, cellPosition.y));
					}
				}
			}
		}

		private void RandomAudioClip()
		{
			if (this.m_EnableAudio)
			{
				int index = Random.Range(0, m_WalkClips.Length);
				m_AudioSource.clip = m_WalkClips[index];
				m_AudioSource.Play();
			}
		}

		public void StartGame()
		{
			this.m_startgame = true;
		}

		public void StopGame()
		{
			this.m_startgame = false;
		}

		public void UpdateSettings()
		{
			if (PlayerPrefs.GetInt(Settings.CONTROLS) == -1)
			{
				m_UseWASD = true;
				for (int i = 0; i < this.transform.childCount; i++)
				{
					this.transform.GetChild(i).gameObject.SetActive(false);
				}
			}
			else if (PlayerPrefs.GetInt(Settings.CONTROLS) == 1)
			{
				m_UseWASD = false;
				for (int i = 0; i < this.transform.childCount; i++)
				{
					this.transform.GetChild(i).gameObject.SetActive(true);
				}
			}

			if (PlayerPrefs.GetInt(Settings.AUDIO) == -1)
			{
				m_EnableAudio = true;
			}
			else if (PlayerPrefs.GetInt(Settings.AUDIO) == 1)
			{
				m_EnableAudio = false;
			}
		}
	}

}
