﻿using PotatoSquash;

using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AmbientNoise : MonoBehaviour
{
	private float timer = 0.1f;
	[SerializeField] private AudioClip[] shoot;
	private AudioSource audioSource;
	private AudioClip shootClip;
	private PlayerMovement m_Player;

	void Start()
	{
		audioSource = this.GetComponent<AudioSource>();
		this.m_Player = PlayerMovement.Instance;
	}
	void Update()
	{
		if (m_Player.m_EnableAudio)
		{

			this.timer -= Time.deltaTime;
			if (timer <= 0)
			{
				int index = Random.Range(0, shoot.Length);
				shootClip = shoot[index];
				audioSource.clip = shootClip;
				audioSource.Play();
				this.timer = 3f;
			}
		}
	}
}
