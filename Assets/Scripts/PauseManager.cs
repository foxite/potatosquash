﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PotatoSquash
{
	public class PauseManager : Singleton<PauseManager>
	{
		[SerializeField] private Animator m_MainMenu;
		private PlayerMovement m_Player;

		public bool Paused { get; private set; }

		private void Start()
		{
			m_Player = PlayerMovement.Instance;
		}

		public void Pause()
		{
			Paused = true;
			m_Player.StopGame();
			m_MainMenu.SetTrigger("Pause");
		}

		public void Unpause()
		{
			Paused = false;
			m_Player.StartGame();
			m_MainMenu.SetTrigger("PauseClose");
		}
	}
}
