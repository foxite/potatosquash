﻿// This was all adapted from the Zelda game from period 3. I haven't checked most of it but it seems to work properly.
// It will make sure that only one instance of the painted prefab exists.

using UnityEditor;
using UnityEngine;

namespace PotatoSquash.Editor
{
	[CreateAssetMenu(menuName = "Brushes/Enemy Spawner")]
	[CustomGridBrush(false, true, false, "Player spawn point")]
	public class PlayerSpawnBrush : GridBrushBase
	{
		public GameObject m_Prefab;
		public int m_ZDepth;

		private GameObject m_CurrentPrefab;

		public override void Paint(GridLayout grid, GameObject brushTarget, Vector3Int pos)
		{
			if (brushTarget.layer == 31) // Layer 31 is used internally
				return;
			
			if (!m_CurrentPrefab)
			{
				m_CurrentPrefab = (GameObject) PrefabUtility.InstantiatePrefab(m_Prefab);
				Undo.RegisterCreatedObjectUndo(m_CurrentPrefab, "Place spawn point");
			}

			m_CurrentPrefab.transform.SetParent(brushTarget.transform);
			m_CurrentPrefab.transform.position = grid.LocalToWorld(grid.CellToLocalInterpolated(new Vector3Int(pos.x, pos.y, m_ZDepth) + new Vector3(.5f, .5f, .5f)));

			SpriteRenderer srcmp;
			if (m_CurrentPrefab.HasComponent(out srcmp))
				srcmp.sortingOrder = m_ZDepth;
		}

		public override void Erase(GridLayout grid, GameObject brushTarget, Vector3Int pos)
		{
			if (brushTarget.layer == 31) // Layer 31 is used internally
				return;

			Transform erased = GetObjectInCell(grid, brushTarget.transform, new Vector3Int(pos.x, pos.y, m_ZDepth));
			if (erased != null)
				Undo.DestroyObjectImmediate(erased.gameObject);
		}

		private static Transform GetObjectInCell(GridLayout grid, Transform parent, Vector3Int pos)
		{
			int childCount = parent.childCount;
			Vector3 min = grid.CellToWorld(pos);
			Vector3 max = grid.CellToWorld(pos + Vector3Int.one);
			Bounds bounds = new Bounds((max + min) * .5f, max - min);

			for (int i = 0; i < childCount; i++)
			{
				Transform child = parent.GetChild(i);
				if (bounds.Contains(child.position))
					return child;
			}
			return null;
		}
	}

	[CustomEditor(typeof(PlayerSpawnBrush))]
	public class PlayerSpawnBrushEditor : GridBrushEditorBase
	{
		private PlayerSpawnBrush TheBrush => target as PlayerSpawnBrush;

		private SerializedObject   m_Object;
		private SerializedProperty m_Prefab;
		private SerializedProperty m_ZDepth;

		protected void OnEnable()
		{
			m_Object = new SerializedObject(target);
			m_Prefab = m_Object.FindProperty("m_Prefab");
			m_ZDepth = m_Object.FindProperty("m_ZDepth");
		}

		public override void OnPaintInspectorGUI()
		{
			m_Object.UpdateIfRequiredOrScript();
			EditorGUILayout.PropertyField(m_Prefab, new GUIContent("Prefab"));

			m_ZDepth = m_Object.FindProperty("m_ZDepth");
			TheBrush.m_ZDepth = EditorGUILayout.IntField("Z Depth", m_ZDepth.intValue);

			m_Object.ApplyModifiedPropertiesWithoutUndo();
		}
	}
}
