﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

namespace PotatoSquash
{
	public class TileCountData : Singleton<TileCountData>
	{
		public const int MaxCount = 3; // Inclusive
		
		[SerializeField] private Tilemap m_Tilemap;
		[SerializeField] private TileBase[] m_Tiles;

		private Animator m_MenuAnimator;
		private PlayerMovement m_Player;
		
		private Dictionary<Vector2Int, int> m_TileCounts; // I hate having a Dictionary with a vector as its key, but this is the best way. https://stackoverflow.com/q/15730346/3141917
		private Dictionary<Vector2Int, int> m_InitialTileCounts; // Used for resetting

		protected override void Awake()
		{
			base.Awake();

			m_Player = PlayerMovement.Instance;

			m_TileCounts = new Dictionary<Vector2Int, int>();

			for (int x = m_Tilemap.cellBounds.xMin; x < m_Tilemap.cellBounds.xMax; x++)
			{
				for (int y = m_Tilemap.cellBounds.yMin; y < m_Tilemap.cellBounds.yMax; y++)
				{
					TileBase tile = m_Tilemap.GetTile(new Vector3Int(x, y, 0));
					if (tile != null)
					{
						int index = Array.IndexOf(m_Tiles, tile);
						if (index >= 0 && index <= MaxCount)
						{
							UpdateTile(new Vector2Int(x, y), index, false);
						}
					}
				}
			}

			m_InitialTileCounts = new Dictionary<Vector2Int, int>(m_TileCounts);
		}

		private void Start()
		{
			GameObject[] resetButtons = GameObject.FindGameObjectsWithTag("ResetButton");
			if (resetButtons.Length != 0)
			{
				foreach (GameObject resetButton in resetButtons)
				{
					resetButton.GetComponent<Button>().onClick.AddListener(ResetGrid);
				}
			}
			else
			{
				Debug.LogWarning("No reset button found");
			}

			GameObject mainMenu = GameObject.FindGameObjectWithTag("MainMenu");
			if (mainMenu != null)
			{
				m_MenuAnimator = mainMenu.GetComponent<Animator>();
			}
			else
			{
				Debug.LogError("No main menu found");
			}
		}

		public void ResetGrid()
		{
			StopAllCoroutines();
			m_TileCounts = new Dictionary<Vector2Int, int>(m_InitialTileCounts);

			for (int x = m_Tilemap.cellBounds.xMin; x < m_Tilemap.cellBounds.xMax; x++)
			{
				for (int y = m_Tilemap.cellBounds.yMin; y < m_Tilemap.cellBounds.yMax; y++)
				{
					if (m_TileCounts.ContainsKey(new Vector2Int(x, y)))
					{
						int newCount = m_TileCounts[new Vector2Int(x, y)];
						TileBase newTile = m_Tiles[newCount];

						UpdateTile(new Vector2Int(x, y), newCount, false);
					}
				}
			}
			m_Player.ResetPlayer();
		}

		/// <summary>
		/// Try to get an existing count. If it does not exist, this returns -1.
		/// </summary>
		public int GetCount(Vector2Int position)
		{
			int count;
			if (m_TileCounts.TryGetValue(position, out count))
			{
				return count;
			}
			else
			{
				return -1;
			}
		}

		public bool CanStepOnTile(Vector2Int position)
		{
			if (m_TileCounts.ContainsKey(position))
			{
				int count = GetCount(position);
				return count >= 0 && count < MaxCount;
			}
			else
			{
				return m_Tilemap.GetTile(Util.V2toV3(position))?.name == "start_tile";
				// the ? means: if m_TileMap.GetTile() is not null, then get its name.
				// Otherwise, it becomes null, and no NullReferenceException is thrown.
			}
		}

		public bool WinCheck()
		{
			bool allTilesThree = true;
			foreach (KeyValuePair<Vector2Int, int> tile in m_TileCounts)
			{
				if (tile.Value != MaxCount)
				{
					allTilesThree = false;
					break;
				}
			}

			if (allTilesThree == true)
			{
				Debug.Log("WON");
				m_MenuAnimator.SetTrigger("Win");
			}

			return allTilesThree;
		}

		public bool LoseCheck()
		{
			bool noWayOut = true;
			Vector3Int playerPosition = m_Tilemap.WorldToCell(m_Player.transform.position);

			foreach (Vector2Int position in Util.FourDirections(Util.V3toV2(playerPosition))) {
				if (CanStepOnTile(position))
				{
					noWayOut = false;
					break;
				}
			}

			if (noWayOut)
			{
				Debug.Log("LOST");
				m_MenuAnimator.SetTrigger("Lose");
			}

			return noWayOut;
		}

		/// <summary>
		/// Increments a count by 1. If it has exceeded the maximum count, the tile will be set to the maximum count.
		/// </summary>
		public void IncrementCount(Vector2Int position)
		{
			if (m_TileCounts.ContainsKey(position))
			{
				int count = m_TileCounts[position] + 1;
				if (count >= MaxCount)
				{
					UpdateTile(position, MaxCount);
				}
				else
				{
					UpdateTile(position, count);
				}
			}
		}

		private void UpdateTile(Vector2Int position, int count, bool checkWinOrLoss = true)
		{
			m_TileCounts[position] = count;
			m_Tilemap.SetTile(new Vector3Int(position.x, position.y, 0), m_Tiles[count]);
			if (count == MaxCount)
			{
				AnimatedTile animTile = m_Tiles[count] as AnimatedTile;

				StartCoroutine(Util.RunSequenceTime(new Action[] {
					() => { m_Tilemap.SetTile(new Vector3Int(position.x, position.y, 0), m_Tiles[4]); },
					() => { m_Tilemap.SetTile(new Vector3Int(position.x, position.y, 0), m_Tiles[5]); },
					() => { m_Tilemap.SetTile(new Vector3Int(position.x, position.y, 0), m_Tiles[6]); }
				}, 1f));
			}

			if (checkWinOrLoss)
			{
				if (!WinCheck())
				{
					// Don't check lose condition if we won (because winning will pass the lose check)
					LoseCheck();
				}
			}
		}
	}
}
