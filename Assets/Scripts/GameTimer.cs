﻿using System.Collections;
using System.Collections.Generic;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace PotatoSquash
{
	public class GameTimer : MonoBehaviour
	{
		private bool isdead = false;

		[SerializeField]
		private float timer = 120f;

		[SerializeField]
		private TextMeshProUGUI timertext;

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{
			if (this.isdead == false)
			{
				this.timer -= Time.deltaTime;
				timertext.text = string.Format("{0:00:00}", this.timer);

			}

			if (timer <= 0)
			{
				this.Endgame();
			}
		}

		void Endgame()
		{
			this.isdead = true;
			Debug.Log("Je bent bad");
		}
	}
}