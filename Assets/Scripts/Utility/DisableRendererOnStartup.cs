﻿// This script will disable a Renderer on startup. This is useful for editor-only sprites.
using UnityEngine;

namespace PotatoSquash.Utility
{
	public class DisableRendererOnStartup : MonoBehaviour
	{
		[SerializeField] private Renderer m_Renderer;

		private void Start()
		{
			m_Renderer.enabled = false;

		}
	}
}
