﻿using UnityEngine;

namespace PotatoSquash.Utility
{
	public class SetAudioMute : MonoBehaviour
	{
		public void SetMute(bool mute)
		{
			AudioListener.volume = mute ? 0 : 1;
		}
	}
}
