﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PotatoSquash
{
	public class PlayerMovementButton : MonoBehaviour
	{
		public UnityEvent on_mouse_button_down;

		[SerializeField] private Color m_colorStandard;
		[SerializeField] private Color m_HoverColor;

		private Material m_hoverColor;

		private void Start()
		{
			m_hoverColor = this.GetComponent<Renderer>().material;
			if (on_mouse_button_down == null)
				on_mouse_button_down = new UnityEvent();

		}

		private void OnMouseDown()
		{
			if (on_mouse_button_down != null)
			{
				this.on_mouse_button_down.Invoke();
			}
		}

		private void OnMouseEnter()
		{
			this.m_hoverColor.color = this.m_HoverColor;

		}

		private void OnMouseExit()
		{
			this.m_hoverColor.color = this.m_colorStandard;
		}
	}
}
