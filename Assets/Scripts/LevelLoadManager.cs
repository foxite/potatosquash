﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PotatoSquash
{
	public class LevelLoadManager : Singleton<LevelLoadManager>
	{
		// Editor
		[SerializeField] private string m_LevelToLoad;
		[SerializeField] private bool m_LoadLevel;

		private string m_CurrentLevel;
		private int? m_CurrentLevelNumber;

		private void Update()
		{
			if (m_LoadLevel)
			{
				m_LoadLevel = false;
				LoadLevel(m_LevelToLoad);
			}
		}

		/// <summary>
		/// Loads a level on top of the current scene by name.
		/// </summary>
		public void LoadLevel(string level)
		{
			m_CurrentLevelNumber = null;
			LoadLevelInternal(level);
		}
		
		/// <summary>
		/// Loads a level by a number.
		/// </summary>
		public void LoadLevel(int levelNumber)
		{
			m_CurrentLevelNumber = levelNumber;
			LoadLevelInternal("Level" + levelNumber.ToString().PadLeft(2, '0'));
		}

		/// <summary>
		/// If the last level was loaded by number, this loads the next level.
		/// </summary>
		public void LoadNextLevel()
		{
			if (m_CurrentLevelNumber != null)
			{
				m_CurrentLevelNumber++;
				LoadLevel(m_CurrentLevelNumber.Value);
			}
			else
			{
				throw new InvalidOperationException("The last level was not loaded by number.");
			}
		}

		/// <summary>
		/// Unloads the current level.
		/// </summary>
		public void UnloadLevel()
		{
			SceneManager.UnloadSceneAsync(m_CurrentLevel);
			m_CurrentLevel = null;
		}

		/// <summary>
		/// Check if a level exists by name.
		/// </summary>
		public bool LevelExists(string level)
		{
			return Application.CanStreamedLevelBeLoaded("Scenes/Levels/" + level);
		}
		
		/// <summary>
		/// Check if a level exists by a number.
		/// </summary>
		public bool LevelExists(int levelNumber)
		{
			return LevelExists("Level" + levelNumber.ToString().PadLeft(2, '0'));
		}

		/// <summary>
		/// If the last level was loaded by number, this checks if the next level exists.
		/// </summary>
		public bool NextLevelExists()
		{
			if (m_CurrentLevelNumber != null)
			{
				return LevelExists(m_CurrentLevelNumber.Value + 1);
			}
			else
			{
				throw new InvalidOperationException("The last level was not loaded by number.");
			}
		}

		private void LoadLevelInternal(string level)
		{
			if (LevelExists(level))
			{
				if (m_CurrentLevel != null)
				{
					UnloadLevel();
				}

				SceneManager.LoadSceneAsync("Scenes/Levels/" + level, LoadSceneMode.Additive);
				m_CurrentLevel = level;
			}
		}
	}
}
