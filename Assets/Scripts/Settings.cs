﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Settings : MonoBehaviour
{
	public const string CONTROLS = "Controls";
	public const string AUDIO = "audio";

	[SerializeField]
	private Toggle controlstoggle;

	[SerializeField]
	private Toggle audiotoggle;
	// Use this for initialization
	void Awake()
	{
		this.controlstoggle.onValueChanged.AddListener(
			delegate
			{
				if (this.controlstoggle.isOn)
				{
					PlayerPrefs.SetInt(CONTROLS, 1);
				}
				else
				{
					PlayerPrefs.SetInt(CONTROLS, -1);

				}


			});
		this.audiotoggle.onValueChanged.AddListener(delegate
		{
			if (this.audiotoggle.isOn)
			{
				PlayerPrefs.SetInt(AUDIO, 1);
			}
			else
			{
				PlayerPrefs.SetInt(AUDIO, -1);

			}
		});
	}

	// Update is called once per frame
	void Start()
	{
		if (PlayerPrefs.GetInt(CONTROLS) == 1)
		{
			controlstoggle.isOn = true;
		}
		else if (PlayerPrefs.GetInt(CONTROLS) == -1)
		{
			controlstoggle.isOn = false;

		}
		if (PlayerPrefs.GetInt(AUDIO) == 1)
		{
			audiotoggle.isOn = true;
		}
		else if (PlayerPrefs.GetInt(AUDIO) == -1)
		{
			audiotoggle.isOn = false;

		}
	}
}
