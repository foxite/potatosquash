﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenes : MonoBehaviour {

	public void PlayGame()
	{
		SceneManager.LoadScene("GameScene");
	}

	public void Settings()
	{
		SceneManager.LoadScene("Settings");
	}
	public void QuitGame()
	{
		Application.Quit();
	}

	public void ReturnToMenu()
	{
		SceneManager.LoadScene("MainMenu");
	}

	public void GoToMainMenu()
	{
		SceneManager.LoadScene("MainMenu");
	}
}
